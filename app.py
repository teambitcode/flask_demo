from flask import Flask, request, jsonify
from flask_pymongo import PyMongo
from bson.json_util import dumps

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://localhost:27017/CRUD_DB"
mongo = PyMongo(app)

@app.route('/create-employee', methods=['POST'])
def createEmployee():
    try:
        id = request.json.get('id')
        name = request.json.get('name')
        age = request.json.get('age')
        salary = request.json.get('salary')
        mongo.db.employee.insert({"id":id,"name" : name,"age" : age,"salary" : salary})
        return jsonify({"status":True,"msg":"successfully created"})
    except :
        return jsonify({"status":False,"msg":"Something went wrong..!"})
        
        
@app.route('/get-all', methods=['GET'])
def getEmployees():
        employees = mongo.db.employee.find()
        jsonOutput = dumps(employees)
        return jsonify({"status":True,"data":jsonOutput})


@app.route('/update-employee', methods=['POST'])
def updateEmployee():
    try:
        id = request.json.get('id')
        name = request.json.get('name')
        age = request.json.get('age')
        salary = request.json.get('salary')
        mongo.db.employee.update(
        {"id":id},{'$set':{"name" : name,"age" : age,"salary" : salary}}
        )
        return jsonify({"status":True,"msg":"successfully updated"})
    except :
        return jsonify({"status":False,"msg":"Something went wrong..!"})


@app.route('/delete-employee/<id>', methods=['GET'])
def deleteEmployee(id):
        employees = mongo.db.employee.delete_one({'id': id})
        return jsonify({"status":True,"msg":"deleted successfully"})
        
        
if __name__ == "__main__":
    app.run(debug=True)
    
    
    